import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_openid import OpenID
from config import basedir

# creates the application object (of class Flask) and then
# imports the views module
app = Flask(__name__)
app.config.from_object('config')  # read config.py file
db = SQLAlchemy(app)

# Models module - database model, where our data are stored
# http://ondras.zarovi.cz/sql/demo/

lm = LoginManager()
lm.init_app(app)
oid = OpenID(app, os.path.join(basedir, 'tmp'))
lm.login_view = 'login'

from app import views, models

